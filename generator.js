function worker(gen, arg) {
  let iter = typeof gen === 'function' ? gen() : gen;
  const current = iter.next(arg);

  if (current.done) {
    return current.value;
  }

  return new Promise(resolve => {
    if (typeof current.value === 'function') {
      resolve(current.value());
    }
    resolve(current.value);
  })
    .then(value => {
      if (value.next) {
        return Promise.resolve(worker(value)).then(val => worker(iter, val));
      }
      return Promise.resolve(value).then(val => worker(iter, val));
    })
    .catch(() => {
      return Promise.resolve(current.value).then(val => worker(iter, val));
    });
}
